export default {

    oidc: {
        clientId: '0oabqjocfvEOoEZQB5d6',
        issuer: 'https://dev-22499051.okta.com/oauth2/default',
        redirectUri: 'http://localhost:4200/login/callback',
        scopes: ['openid', 'profile', 'email']
    }

}
